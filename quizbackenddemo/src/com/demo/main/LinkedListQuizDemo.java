package com.demo.main;

public class LinkedListQuizDemo {	  
    //Represent a node of the singly linked list  
    class Node{  
        int data;  
        Node next;  
  
        public Node(int data) {  
            this.data = data;  
            this.next = null;  
        }  
    }  
  
    //Represent the head and tail of the singly linked list  
    public Node head = null;  
    public Node tail = null;  
  
    //addNode() will add a new node to the list  
    public void addNode(int data) {  
        //Create a new node  
        Node newNode = new Node(data);  
  
        //Checks if the list is empty  
        if(head == null) {  
            //If list is empty, both head and tail will point to new node  
            head = newNode;  
            tail = newNode;  
        }  
        else {  
            //newNode will be added after tail such that tail's next will point to newNode  
            tail.next = newNode;  
            //newNode will become new tail of the list  
            tail = newNode;  
        }  
    }  
  
    //removeTail() will delete a node from end of the list  
    public void removeTail() {  
  
        //Checks if the list is empty  
        if(head == null) {  
            System.out.println("List is empty");  
            return;  
        }  
        else {  
            //Checks whether the list contains only one element  
            if(head != tail ) {  
                Node current = head;  
                //Loop through the list till the second last element such that current.next is pointing to tail  
                while(current.next != tail) {  
                    current = current.next;  
                }  
                //Second last element will become new tail of the list  
                tail = current;  
                tail.next = null;  
            }  
            //If the list contains only one element  
            //Then it will remove it and both head and tail will point to null  
            else {  
                head = tail = null;  
            }  
        }  
    } 
    
    
    
    
    
    private  Node removeNodes(Node start, int x) {

    	if(start == null) return start;

    	if(start.data > x && start.next == null) return null;

    	//find first head node
    	Node cur = start;
    	Node prev = null;

    	//4,5,3,2,1,6 --- where x = 2
    	while(cur != null && cur.data > x) {
    	    prev = cur;
    	    cur = cur.next;
    	}

    	if(prev != null) prev.next = null;

    	Node newHead = cur;

    	while(cur.next != null) {
    	    if(cur.next.data > x) {
    	        cur.next = cur.next.next;
    	    } else {
    	        cur = cur.next;
    	    }
    	}

    	return newHead;
    	}
    
  
    //display() will display all the nodes present in the list  
    public void display() {  
        //Node current will point to head  
        Node current = head;  
        if(head == null) {  
            System.out.println("List is empty");  
            return;  
        }  
        while(current != null) {  
            //Prints each node by incrementing pointer  
            System.out.print(current.data + " ");  
            current = current.next;  
        }  
        System.out.println();  
    }  
  
    public static void main(String[] args) {  
    	int target=3;
  
    	LinkedListQuizDemo sList = new LinkedListQuizDemo();  
  
        //Adds data to the list  
        sList.addNode(1);  
        sList.addNode(2);  
        sList.addNode(3);  
        sList.addNode(4);  
  
        //Printing original list  
        System.out.println("Original List: ");  
        sList.display();  
        
        System.out.println("Appending item in list....... "); 
        sList.addNode(5);
        sList.addNode(6);
        sList.addNode(7);
        
        System.out.println("Appended List: ");  
        sList.display(); 
  
        if(sList.head != null) {  
            sList.removeTail();  
            //Printing updated list  
            System.out.println("Removed tail: ");  
            sList.display();  
        } 
        
        if(sList.head != null) {        
        System.out.println("Removed all element in the linkedlist that is great than a target value. ");  
        System.out.println("Target value is:- "+target);
        sList.removeNodes(sList.head, target);
        sList.display();
        }
        
    }  
}
